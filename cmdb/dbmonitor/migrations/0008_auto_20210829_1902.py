# Generated by Django 2.0.6 on 2021-08-29 19:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dbmonitor', '0007_auto_20210829_1743'),
    ]

    operations = [
        migrations.AddField(
            model_name='mysqlhistory',
            name='key_blocks_used_rate',
            field=models.FloatField(blank=True, null=True, verbose_name='key块使用率'),
        ),
        migrations.AddField(
            model_name='mysqlhistory',
            name='key_buffer_read_rate',
            field=models.FloatField(blank=True, null=True, verbose_name='key读请求命中率'),
        ),
        migrations.AddField(
            model_name='mysqlhistory',
            name='key_buffer_write_rate',
            field=models.FloatField(blank=True, null=True, verbose_name='key写请求命中率'),
        ),
    ]

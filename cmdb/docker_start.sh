#!/bin/bash
#coding=utf-8
#auth:郝晨霄


port=12000
nowpath=/ncc/service/fsr
#进入虚拟环境
source /usr/local/env/bin/activate
export LANG="zh_CN.utf8"
starts()
{
#启动fsr服务
cd $nowpath/cmdb
nohup python manage.py runserver 0.0.0.0:${port} >>../logs/fsr.log 2>&1 &
echo "启动fsr服务"
#启动自愈服务
cd ../butterfly
nohup python AutoPlay.py >>logs/butterfly.log 2>&1 &
echo "启动自愈服务"
#启动celery异步调度服务 
rm -f celery.pid
celery multi start w1 -A tasks -c 4 --loglevel=info --logfile=logs/celery.log --pidfile=celery.pid
echo "启动celery异步调度服务"
tail -f AutoPlay.py
}


stops()
{

cd $nowpath
#关闭fsr服务
ps -elf |grep ${port}|grep -v grep |awk '{print $4}'|xargs kill
echo "关闭fsr服务"
#关闭自愈服务
ps -elf |grep AutoPlay.py |grep -v grep |awk '{print $4}'|xargs kill
echo "关闭自愈服务"
cd ../butterfly
#停止celery异步调度服务
celery multi stop w1  -A tasks -c 4 --loglevel=info
rm -f celery.pid
ps -elf|grep fsr|grep celery|awk '{print $4}'|xargs kill 2>/dev/null
echo "停止celery异步调度服务"
}

statuss()
{

fsr_num=`ps -elf |grep ${port}|grep -v grep |wc -l`
if [ $fsr_num -ge 1 ];then
    echo "fsr服务正在运行"
else
    echo "fsr服务停止"
fi

ac_num=`ps -elf |grep AutoPlay.py |grep -v grep |wc -l`
if [ $ac_num -ge 1 ];then
    echo "自愈服务正在运行"
else
    echo "自愈服务停止"
fi

if [ -f ../butterfly/celery.pid ];then
    echo "celery异步调度服务正在运行"
else
    echo "celery异步调度服务已停止"
fi
}

case $1 in 
start)
    starts;;
stop)
    stops;;
restart)
    stops && starts;;
status)
    statuss;;
*)
    echo "请输入start/stop/restart"
esac 

#重启celery
#celery multi restart w1  -A tasks -c 4 --loglevel=info
